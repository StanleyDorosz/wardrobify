/*we need to grab all of the pictures from hats and shoes and then put then in
corousels side by side. We need to set the corousels side by side to use up max space on
the home page. We'll also need a fetchData function to grab data (the pictuers) from the
shoes api as well as the hats api. */

import { useEffect, useState } from "react";
import Carousel from 'react-bootstrap/Carousel'


function MainPage() {
  const [shoesPicturesList, setShoesPicturesList] = useState([]);
  const [hatsPicturesList, setHatsPicturesList] = useState([]);

  const fetchData = async () => {
    const shoesUrl = "http://localhost:8080/api/shoes/";
    const response = await fetch(shoesUrl);
    if (response.ok) {
      const data = await response.json();
      const shoesPicturesList = data.shoes.map((shoe) => {
        return shoe.picture_url;
      });
      setShoesPicturesList(shoesPicturesList);
    }
    const hatsUrl = "http://localhost:8090/api/hats";
    const response2 = await fetch(hatsUrl);
    if (response2.ok) {
      const data = await response2.json();
      const hatsPicturesList = data.hats.map((hat) => {
        return hat.picture_url;
      });
      setHatsPicturesList(hatsPicturesList);
    }

  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
      <div className="px-4 py-5 my-5 text-center">
        {/* <h1 className="display-5 fw-bold">WARDROBIFY!</h1> */}
        <img className="img" src={require("./title.png")} alt="Wardrobify"/>
        <div className="col-lg-6 mx-auto">
          <p className="lead mb-4">
            Need to keep track of your shoes and hats? We have
            the solution for you!
          </p>
        </div>
      </div>
      <div className="container">
        <div className="row">
          <div className="col-6 off-set-1">
            <Carousel>
                {shoesPicturesList.map((picture_url, index) => {
                  return (
                    <Carousel.Item key={picture_url}>
                      <img className="d-block w-100 img-fluid" src={picture_url} alt={`${index} Slide`} />
                    </Carousel.Item>
                  )
                })}
            </Carousel>
          </div>
          <div className="col-6 off-set-1">
            <Carousel>
                {hatsPicturesList.map((picture_url, index) => {
                  return (
                    <Carousel.Item key={picture_url}>
                      <img className="d-block w-100 img-fluid" src={picture_url} alt={`${index} Slide`} />
                    </Carousel.Item>
                  )
                })}
            </Carousel>
          </div>
        </div>
      </div>
    </>
  );
}

export default MainPage;
