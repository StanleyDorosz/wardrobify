import { useEffect, useState } from "react";


const deleteHat = async (event) => {
    const deleteUrl = `http://localhost:8090/api/hats/${event.target.attributes.id.value}/`;
    const fetchOptions = {
        method: "delete",
    }
    const response = await fetch(deleteUrl, fetchOptions);
    if (response.ok) {
        const data = await response.json();
        window.location.reload();
    } else {
        console.error("Bad Response", response.status);
    }
}


function HatCard (props) {
    const hat = props.hat;
    return (
        <div key={hat.id} className="card mb-3 shadow p-3">
            <img src={hat.picture_url} className="card-img-top" alt={`Fabric: ${hat.fabric}, style: ${hat.style_name}, color: ${hat.color}`} />
            <div className="card-body">
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Details:</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Fabric</td>
                            <td>{hat.fabric}</td>
                        </tr>
                        <tr>
                            <td>Style</td>
                            <td>{hat.style_name}</td>
                        </tr>
                        <tr>
                            <td>Color</td>
                            <td>{hat.color}</td>
                        </tr>
                        <tr>
                            <td>Closet</td>
                            <td>{hat.location.closet_name}</td>
                        </tr>
                        <tr>
                            <td>Section</td>
                            <td>{hat.location.section_number}</td>
                        </tr>
                        <tr>
                            <td>Shelf</td>
                            <td>{hat.location.shelf_number}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div className="card-footer text-center">
                <button type="button" variant="contained" className="btn btn-danger" id={hat.id} onClick={deleteHat}>
                    Delete
                </button>
            </div>
        </div>
    )
}


function HatColumn(props) {
    return (
        <div className="col">
            {props.list.map((hat, index) => {
                return (
                    <HatCard key={index} hat={hat} />
                )
            })}
        </div>
    )
}

const HatList = () => {

    const [hatColumns, setHatColumns] = useState([[], [], []])


    const fetchData = async () => {

        const url = "http://localhost:8090/api/hats/";
        try {
            const response = await fetch(url);

            const columns = [[], [], []];
            if (response.ok) {
                const data = await response.json();
                let index = 0;
                for (const hat of data.hats) {
                    columns[index].push(hat);
                    index += 1;
                    if (index > 2) {
                        index = 0;
                    }
                }
            }
            setHatColumns(columns);
        } catch (error) {
            console.error(error);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="container">
            <div className="row">
                <div className="col col-12">
                    <div className="card text-center shadow-light mb-3">
                        <h2 className="card-title">Hats</h2>
                    </div>
                </div>
            </div>
            <div className="row">
                {hatColumns.map((hatColumnList, index) => {
                    return (
                        <HatColumn key={index} list={hatColumnList} />
                    )
                })}
            </div>
        </div>
    );
}
export default HatList;
