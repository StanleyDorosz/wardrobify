
function Footer() {
  return (
    <div className="container">
  <footer className="d-flex flex-wrap justify-content-between align-items-center py-3 my-4 border-top">
    <p className="col-md-4 mb-0 text-body-secondary">Created by Nathan Batten & Stanley Dorosz</p>
    <ul className="nav col-md-4 justify-content-end">
      <li className="nav-item"><a href="#" className="nav-link px-2 text-body-secondary">Back To Top</a></li>
      <li className="nav-item"><a href="/" className="nav-link px-2 text-body-secondary">Home</a></li>
    </ul>
  </footer>
</div>
  )
}

export default Footer;
