import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoeList from './ShoeList';
import NewShoeForm from './NewShoeForm';
import HatList from './HatList';
import NewHatForm from './NewHatForm';
import Footer from './Footer';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes/">
            <Route index={true} element={<ShoeList />} />
            <Route path="new/" element={<NewShoeForm />} />
            </Route>
          <Route path="hats/">
            <Route index element={<HatList />} />
            <Route path="new/" element={<NewHatForm />} />
          </Route>
        </Routes>
      </div>
      <Footer />
    </BrowserRouter>
  );
}

export default App;
