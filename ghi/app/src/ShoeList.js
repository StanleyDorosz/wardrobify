import { useEffect, useState } from "react";

const deleteShoe = async (event) => {
    const deleteUrl = `http://localhost:8080/api/shoes/${event.target.attributes.id.value}/`;
    const fetchOptions = {
        method: "delete",
    }
    const response = await fetch(deleteUrl, fetchOptions);
    if (response.ok) {
        const data = await response.json();
        console.log(data);
        window.location.reload();
    } else {
        console.error("Bad Response", response.status);
    }
}


function ShoeCard (props) {
    const shoe = props.shoe;
    return (
        <div key={shoe.id} className="card mb-3 shadow">
            <img src={shoe.picture_url} alt={`${shoe.model_name} Shoe`} className="card-img-top"/>
            <div className="card-body">
                <ul>
                    <li>Manufacturer: {shoe.manufacturer}</li>
                    <li>Model: {shoe.model_name}</li>
                    <li>Color: {shoe.color}</li>
                    <li>Closet: {shoe.bin.closet_name}</li>
                    <li>Bin Number: {shoe.bin.bin_number}</li>
                    <li>Bin Size: {shoe.bin.bin_size}</li>
                </ul>
            </div>
            <div className="card-footer text-center">
                <button type="button" variant="contained" className="btn btn-danger" id={shoe.id} onClick={deleteShoe}>
                    Delete
                </button>
            </div>
        </div>
    )
}


function ShoeColumn(props) {
    return (
        <div className="col">
            {props.list.map((shoe, index) => {
                return (
                    <ShoeCard key={index} shoe={shoe} />
                )
            })}
        </div>
    )
}

const ShoeList = () => {

    const [shoeColumns, setShoeColumns] = useState([[], [], []])

    const fetchData = async () => {

        const url = "http://localhost:8080/api/shoes/";
        try {
            const response = await fetch(url);

            const columns = [[], [], []];
            if (response.ok) {
                const data = await response.json();
                let index = 0;
                for (const shoe of data.shoes) {
                    columns[index].push(shoe);
                    index += 1;
                    if (index > 2) {
                        index = 0;
                    }
                }
            }
            setShoeColumns(columns);
        } catch (error) {
            console.error(error);
        }
    }


    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="container">
            <div className="row">
            <div className="col col-12">
                    <div className="card text-center shadow-light mb-3">
                        <h2 className="card-title">Shoes</h2>
                    </div>
                </div>
                {shoeColumns.map((shoeColumnList, index) => {
                    return (
                        <ShoeColumn key={index} list={shoeColumnList} />
                    )
                })}
            </div>
        </div>
    );
}
export default ShoeList;
