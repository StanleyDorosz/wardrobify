import React, { useEffect, useState } from 'react';

// don't need props because already grabbing data in the function

function NewShoeForm() {
    const [states, setStates] = useState([]);
    // Set the useState hook to store "Manufacturer" in the component's picture_url,
    // with a default initial value of an empty string.
    const [manufacturer, setManufacturer] = useState('');
    const [model_name, setModelName] = useState('');
    const [color, setColor] = useState('');
    const [picture_url, setPictureUrl] = useState('');
    const [bin, setBin] = useState('');
    // Create the handleManufacturerChange method to take what the user inputs
    // into the form and store it in the picture_url's "Manufacturer" variable.
    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const handleModelNameChange = (event) => {
        const value = event.target.value;
        setModelName(value);
    }

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handlePictureUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }

    const handleBinChange = (event) => {
        const value = event.target.value;
        setBin(value);
    }



    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
            setStates(data.bins);

          }
        }

      useEffect(() => {
        fetchData();
      }, []);

      const handleSubmit = async (event) => {
        event.preventDefault();

        // create an empty JSON object
        const data = {};

        data.model_name = model_name;
        data.manufacturer = manufacturer;
        data.color = color;
        data.picture_url = picture_url;
        data.bin = bin;


        const newShoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
        'Content-Type': 'application/json',
        },
    };
        const response = await fetch(newShoeUrl, fetchConfig);
        if (response.ok) {
        const newShoe = await response.json();
        console.log(newShoe);

        setManufacturer('');
        setModelName('');
        setColor('');
        setPictureUrl('');
        setBin('');
        };
      };

    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new shoe</h1>
                        <form onSubmit={handleSubmit} id="create-shoe-form">
                            <div className="form-floating mb-3">
                            <input onChange={handleManufacturerChange} value={manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                            <label htmlFor="manufacturer">Manufacturer</label>
                            </div>
                            <div className="form-floating mb-3">
                            <input onChange={handleModelNameChange} value={model_name} placeholder="Model name" required type="text" name="model_name" id="model_name" className="form-control"/>
                            <label htmlFor="model_name">Model name</label>
                            </div>
                            <div className="form-floating mb-3">
                            <input onChange={handleColorChange} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                            <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                            <input onChange={handlePictureUrlChange} value={picture_url} placeholder="Picture url" required type="url" name="picture_url" id="picture_url" className="form-control"/>
                            <label htmlFor="picture_url">Picture url</label>
                            </div>
                            <div className="form-floating mb-3">
                                <select onChange={handleBinChange} value={bin} required id="bin" name="bin" className="form-select">
                                    <option value="">Choose a bin</option>
                                    {states.map(bin => {
                                        return (
                                            <option key={bin.id} value={bin.href}>
                                                {bin.closet_name}
                                            </option>
                                        );

                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default NewShoeForm;
