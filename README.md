# Wardrobify
>An Application for Organizing and Storing Hats and Shoes

Team:

* Nathan Batten - Hats Microservice
* Stanley Dorosz - Shoes Microservice

## Design
![alt text](Wardrobify-Diagram.png)



## Shoes microservice

<!-- Explain your models and integration with the wardrobe
microservice, here. -->

The first thing I did was register my Django app into my Django project (INSTALLED_APPS) by looking at my apps.py file for reference. Created my models, and migrated these changes. Then, to start I used `/shoes/poll/poller.py`. This poller makes an API call to the wardrobe's bin API and get all of the bins from it and converts all those bins into BinVO's (Bin Value Objects.)
Every 60 seconds this API call will be made to ensure the data being pulled from the wardrobe API is up to date. Then in `shoes/api/shoes_rest/api_views.py` I set up my own API using RESTFUL standards so that it would handle `GET`, `POST`, `DELETE`, and `PUT` requests. I used fearless front end and conference go as reference. I then set up my urls so that the back end could have its paths to route to.

When setting up my urls, I feel it is important to note that I changed the name of the `urls.py` file to `api_urls.py` to let all developers and users etc. know that this `py file` is specific to APIs, (I also did this for my `views.py` file.) Thus, enabling the browser to act as a middleman between the front and back end. Then, Using Insomnia to simulate the front end, I tested these API calls, getting a list of all bins (that I created), and a list of all shoes (that I created.) Created some bins and shoes with the `POST` method and then tested my `PUT` API call by updating some of those specific bins and shoes.

Also tested my `DELETE` API call as well which returns either true or false depending on whether that specific instance was deleted. I also edited the `admin.py` file to enable me to be able to easily add, and delete shoes and bins in a more visually pleasing fashion. (seeing my data in the admin helps me to visualize what is going on, whereas insomnia is simulating the front end aspect.) I then included the URLs from my Django app in my Django project URLs.

BinVO was used as a foreign key to my Shoe model as well. this was important in the process of making requests. it enabled me to create instances of the Bin model from the wardrobe API this is why I made Bin a VO, because I didn't want its properties to be mutable. (after I create it I don't want it be be changed per say.)

## Hats microservice

<!-- Explain your models and integration with the wardrobe
microservice, here. -->
<!-- It all starts on a dark and stormy night (Ok yeah it was mid-day but surely if you're reading the comments you can understand a metaphor) when I was given this assignment by a group of cruel (I mean smart and kind if they're reading this which they probably will be) SEIRS and Jay. -->

 To start I implemented `/hats/poll/poller.py`. The Poller makes an API call to wardrobe's locations api and gets all of the locations from it and converts them into Location Value Objects. This allows my hats microservice to get data for Locations without exceeded it's bouded context. In `/hats/api/hats_rest/api_views.py` I set up an API with RESTful standards so that it can handle `Get`, `POST`, `DELETE`, and `PUT` requests (though the `PUT` functionality is not currently used).
