from django.contrib import admin
from .models import BinVO, Shoe
# Register your models here.

@admin.register(BinVO)
class BinVOAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "import_href",
        "closet_name",
        "bin_number",
        "bin_size",

    ]


@admin.register(Shoe)
class ShoeAdmin(admin.ModelAdmin):
    list_display = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin",
    ]
